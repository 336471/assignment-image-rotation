#include "utils.h"
#include "io.h"

#include "bmp_work.h"
#include <stdlib.h>

static bool check_signature(const struct bmp_header* bmpHeader) {
    const char* bmpSignature = "BM";
    if (bmpHeader->bfType == *((uint16_t*) bmpSignature)) {
        return true;
    }
    return false;
}

uint64_t get_width_with_padding(uint64_t width) {
    if (width % 4 != 0) {
        width += 4 - (width % 4);
    }
    return width;
}

static struct bmp_header create_bmp_header(const struct image* img) {
    struct bmp_header bmpHeader = {0};
    bmpHeader.bfType = *(uint16_t*)
            "BM";
    bmpHeader.bOffBits = 54;
    bmpHeader.biWidth = img->width;
    bmpHeader.biHeight = img->height;

    uint64_t width_with_padding = get_width_with_padding(img->width * sizeof(struct pixel));
    bmpHeader.bfileSize = bmpHeader.bOffBits + width_with_padding * bmpHeader.biHeight;
    bmpHeader.biSize = 40;
    bmpHeader.biPlanes = 1;
    bmpHeader.biBitCount = 8 * sizeof(struct pixel);
    bmpHeader.biCompression = 0;
    bmpHeader.biSizeImage = 0;
    bmpHeader.biXPelsPerMeter = 0;
    bmpHeader.biYPelsPerMeter = 0;
    bmpHeader.biClrUsed = 0;
    bmpHeader.biClrImportant = 0;

    return bmpHeader;
}

enum read_status from_bmp(FILE* in, struct image* img) {

    const int header_size = sizeof(struct bmp_header);
    char* header_buffer = malloc(sizeof(char) * header_size);

    const size_t size = fread(header_buffer, 1,  sizeof(struct bmp_header), in);

    if (size != sizeof(struct bmp_header)) {
        free(header_buffer);
        return READ_INVALID_SIGNATURE;
    }

    const struct bmp_header* bmpHeader = (const struct bmp_header*) header_buffer;

    if (!check_signature(bmpHeader)) {
        free(header_buffer);
        return READ_INVALID_SIGNATURE;
    }

    const uint64_t width = bmpHeader->biWidth;
    const uint64_t width_in_bytes = width * sizeof(struct pixel);
    const uint64_t height = bmpHeader->biHeight;

    const uint64_t width_with_padding = get_width_with_padding(width_in_bytes);

    char* data_buffer = malloc(sizeof(char) * width_with_padding * height);

    fseek(in, bmpHeader->bOffBits, SEEK_SET);
    const size_t data_size = fread(data_buffer, 1, width_with_padding * height, in);

    if (data_size != width_with_padding * height) {
        free(header_buffer);
        free(data_buffer);
        return READ_INVALID_BITS;
    }

    *img = create_image(width, height, data_buffer);

    free(header_buffer);
    free(data_buffer);

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {

    struct bmp_header bmpHeader = create_bmp_header(img);
//    printf("%d\n", bmpHeader.bfileSize);
    const uint64_t width_with_padding = get_width_with_padding(img->width * sizeof(struct pixel));

    char* buff = malloc(bmpHeader.bfileSize);
    my_memcpy(buff, &bmpHeader, sizeof(struct bmp_header));

    for (uint64_t h = 0; h < img->height; ++h) {
        for (uint64_t w = 0; w < width_with_padding; ++w) {
            if (w < img->width * sizeof(struct pixel)) {
                buff[bmpHeader.bOffBits + width_with_padding * h + w] = ((char*) img->data)[
                        img->width * sizeof(struct pixel) * h + w];
            } else {
                buff[bmpHeader.bOffBits + width_with_padding * h + w] = 0;
            }
        }
    }

    size_t size = write_file(out, bmpHeader.bfileSize, buff);
    if (size != bmpHeader.bfileSize) {
        free(buff);
        return WRITE_ERROR;
    }

    free(buff);
    return WRITE_OK;
}
