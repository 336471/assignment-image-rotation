#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool open_file(const char* filename, const char* mode, FILE** file);
void close_file(FILE* file);

size_t get_file_size(FILE* file);
size_t read_file(FILE* file, size_t size, char* buff);
size_t write_file(FILE* file, size_t size, char* buff);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
