
#include "rotate.h"
#include <stdlib.h>

struct image rotate(const struct image* img){
    struct image new_img;

    new_img.height = img->width;
    new_img.width = img->height;

    new_img.data = malloc(sizeof(struct pixel) * new_img.height * new_img.width);
    for (uint64_t h = 0; h < new_img.height; ++h){
        for (uint64_t w = 0; w < new_img.width; ++w){
            new_img.data[new_img.width * h + new_img.width - w - 1] = img->data[new_img.height * w + h];
        }
    }

    return new_img;
}
