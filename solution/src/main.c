#include "bmp_work.h"
#include "image.h"
#include "io.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>



int main(int argc, char** argv) {

    if (argc < 3) {
        exit(1);
    }
    FILE* file = NULL;

    open_file(argv[1], "rb", &file);

    struct image img;
    enum read_status r = from_bmp(file, &img);
    if (r != 0) {
        exit(r);
    }

    struct image new_image = rotate(&img);

    FILE* out;
    open_file(argv[2], "wb", &out);

    to_bmp(out, &new_image);
    free(img.data);
    free(new_image.data);
    close_file(file);
    close_file(out);
}
