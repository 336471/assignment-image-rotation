#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_WORK_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_WORK_H

#include "enums.h"
#include "image.h"
#include <stdio.h>

uint64_t get_width_with_padding(uint64_t width);
enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, const struct image* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_WORK_H
