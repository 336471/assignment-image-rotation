
#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image.h"
#include "utils.h"

struct image rotate(const struct image* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
