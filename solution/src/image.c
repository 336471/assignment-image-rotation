//
// Created by qbic on 18.01.23.
//

#include "bmp_work.h"
#include "image.h"
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height, char* raw_raster) {
    char* data = malloc(3 * width * height);
    const uint64_t width_with_padding = get_width_with_padding(width * 3);
    for (uint64_t h = 0; h < height; ++h) {
        for (uint64_t w = 0; w < width_with_padding; ++w) {
            if (w < width * sizeof(struct pixel)) {
                data[width * sizeof(struct pixel) * h + w] = raw_raster[width_with_padding * h + w];
            }
        }
    }

    return (struct image) {.width = width, .height = height, .data = (struct pixel*) data};
}
