
#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include "enums.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>

inline void my_memcpy(void* dst, const void* src, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        ((char*) dst)[i] = ((const char*) src)[i];
    }
}


#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
