#include "io.h"

bool open_file(const char* filename, const char* mode, FILE** file) {
    *file = fopen(filename, mode);
    if (*file == NULL) {
        return false;
    }
    return true;
}

void close_file(FILE *file) {
    if (file == NULL) {
        return;
    }
    fclose(file);
}

size_t get_file_size(FILE *file) {
    if (file == NULL) {
        exit(-1);
    }

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, 0);

    return size;
}

size_t read_file(FILE *file, size_t size, char *buff) {
    return fread(buff, 1, size, file);
}

size_t write_file(FILE* file, size_t size, char* buff){
    return fwrite(buff, size, 1, file);
}

#include "io.h"
