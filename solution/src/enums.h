#ifndef ASSIGNMENT_IMAGE_ROTATION_ENUMS_H
#define ASSIGNMENT_IMAGE_ROTATION_ENUMS_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};


#endif //ASSIGNMENT_IMAGE_ROTATION_ENUMS_H
